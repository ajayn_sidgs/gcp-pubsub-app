# Create GKE  on GCP 

Ref: 
- https://cloud.google.com/architecture/integrating-microservices-with-pubsub
- https://zoltanaltfatter.com/2019/08/16/dockerizing-spring-boot-apps-with-jib/

Create a GKE Cluster 

```
gcloud container clusters create "hsl-poc-cluster" \
    --scopes "https://www.googleapis.com/auth/cloud-platform" \
    --num-nodes "2" --region us-central1 --project seismic-lexicon-195122
```

## Get creds 

```
gcloud container clusters get-credentials hsl-poc-cluster --project seismic-lexicon-195122 --region us-central1

```
## Build the project 

```
export PROJECT_ID=seismic-lexicon-195122

gcloud builds submit ./pubsub-app -t \
    gcr.io/${PROJECT_ID}/pubsub-app
```