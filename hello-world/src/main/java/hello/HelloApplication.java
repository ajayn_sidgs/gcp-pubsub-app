package hello;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;



@SpringBootApplication
public class HelloApplication {

  private static final Log LOGGER = LogFactory.getLog(HelloApplication.class);

  public static void main(String[] args) throws IOException {
	SpringApplication.run(HelloApplication.class, args);
  }


}
